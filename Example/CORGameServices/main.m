//
//  main.m
//  CORGameServices
//
//  Created by Damian Grzybinski on 05/05/2022.
//  Copyright (c) 2022 Damian Grzybinski. All rights reserved.
//

@import UIKit;
#import "CORGAMESERVICESAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CORGAMESERVICESAppDelegate class]));
    }
}
