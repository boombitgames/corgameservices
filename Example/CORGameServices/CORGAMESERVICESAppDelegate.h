//
//  CORGAMESERVICESAppDelegate.h
//  CORGameServices
//
//  Created by Damian Grzybinski on 05/05/2022.
//  Copyright (c) 2022 Damian Grzybinski. All rights reserved.
//

@import UIKit;

@interface CORGAMESERVICESAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
