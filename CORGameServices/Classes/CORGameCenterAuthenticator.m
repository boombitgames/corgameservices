#import <GameKit/GameKit.h>


typedef void (*FetchItemsCallback)(const char * publicKeyUrl, const char * signature, int signatureLength, const char * salt, int saltLength, const uint64_t timestamp, const char * error);

extern void fetchItems(FetchItemsCallback callback)
{
    GKLocalPlayer * localPlayer = [GKLocalPlayer localPlayer];
    
    if (@available(iOS 13.5, *))
    {
        [localPlayer fetchItemsForIdentityVerificationSignature:^(NSURL * _Nullable publicKeyURL, NSData * _Nullable signature, NSData * _Nullable salt, uint64_t timestamp, NSError * _Nullable error)
        {
            // Create a pool for releasing the resources we create
            @autoreleasepool
            {
            
                // PublicKeyUrl
                const char * publicKeyUrlCharPointer = NULL;
                if (publicKeyURL != NULL)
                {
                    const NSString * publicKeyUrlString = [[NSString alloc] initWithString:[publicKeyURL absoluteString]];
                    publicKeyUrlCharPointer = [publicKeyUrlString UTF8String];
                }
                
                // Signature
                const char * signatureBytes = [signature bytes];
                int signatureLength = (int)[signature length];
                
                // Salt
                const char * saltBytes = (char *)[salt bytes];
                int saltLength = (int)[salt length];
                
                // Error
                const NSString * errorString = error.description;
                const char * errorStringPointer = [errorString UTF8String];

                // Callback
                callback(publicKeyUrlCharPointer, signatureBytes, signatureLength, saltBytes, saltLength, timestamp, errorStringPointer);
            }
        }];
    }
    else
    {
        // Fallback on earlier versions
        [localPlayer generateIdentityVerificationSignatureWithCompletionHandler:^(NSURL * publicKeyUrl, NSData * signature, NSData * salt, uint64_t timestamp, NSError * error) {

            // Create a pool for releasing the resources we create
            @autoreleasepool {
                
                // PublicKeyUrl
                const char * publicKeyUrlCharPointer = NULL;
                if (publicKeyUrl != NULL)
                {
                    const NSString * publicKeyUrlString = [[NSString alloc] initWithString:[publicKeyUrl absoluteString]];
                    publicKeyUrlCharPointer = [publicKeyUrlString UTF8String];
                }

                // Signature
                const char * signatureBytes = [signature bytes];
                int signatureLength = (int)[signature length];

                // Salt
                const char * saltBytes = [salt bytes];
                int saltLength = (int)[salt length];

                // Error
                const NSString * errorString = error.description;
                const char * errorStringPointer = [errorString UTF8String];

                // Callback
                callback(publicKeyUrlCharPointer, signatureBytes, signatureLength, saltBytes, saltLength, timestamp, errorStringPointer);
            }
        }];
    }
}

