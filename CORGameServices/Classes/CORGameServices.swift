//
//  CORGameServices.swift
//  CORGameServices
//

import Foundation
import GameKit

public class CORGameServices : NSObject, GKGameCenterControllerDelegate {
    private var Achievements: [GKAchievement]? = []
    
    public func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController)
    {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    
    @objc public static let shared = CORGameServices()
    
    @objc public func login (callback: @escaping (Int32) -> Void)
    {
        let localPlayer = GKLocalPlayer.local
        localPlayer.authenticateHandler = { viewController, error in
            if let viewController = viewController {
                UnityGetGLViewController().present(viewController, animated: true, completion: {
                    if(localPlayer.isAuthenticated)
                    {
                        callback(1)
                        return
                    }
                    else
                    {
                        callback(0)
                        return
                    }
                })
                return;
            }
            if error != nil {
                DispatchQueue.main.async {
                    callback(0)
                    return
                }
                return;
            }
            else
            {
                if(localPlayer.isAuthenticated)
                {
                    callback(1)
                    return
                }
                else
                {
                    callback(0)
                    return
                }
            }
        }
    }
    
    @objc public func isLoggedIn () -> Bool
    {
        return GKLocalPlayer.local.isAuthenticated
    }
    
    @objc public func getDisplayName () -> String
    {
        return GKLocalPlayer.local.displayName
    }
    
    @objc public func getPlayerID () -> String
    {
        return GKLocalPlayer.local.playerID
    }
    
    @objc public func getGamePlayerID () -> String
    {
        if #available(iOS 12.4, *) {
            return GKLocalPlayer.local.gamePlayerID
        } else {
            return "no_game_player_id"
        }
    }
    
    @objc public func getTeamPlayerID () -> String
    {
        if #available(iOS 12.4, *) {
            return GKLocalPlayer.local.teamPlayerID
        } else {
            return "no_team_player_id"
        }
    }
    
    @objc public func reportAchievementProgress (achievementId:String, progress:Double)
    {
        var achievement: GKAchievement? = nil
        achievement = GKAchievement(identifier: achievementId)
        achievement?.percentComplete = progress
        
        let achievementsToReport: [GKAchievement] = [achievement!]
        
        
        GKAchievement.report(achievementsToReport, withCompletionHandler: {(error: Error?) in
            if error != nil {
                // Handle the error that occurs.
                print("Error while reporting achievement: \(String(describing: error))")
            }
        })
    }
    
    @objc public func CORResetAllAchievements()
    {
        GKAchievement.resetAchievements()
    }
    
    @objc public func CORShowAchievements()
    {
        let viewController : GKGameCenterViewController
        if #available(iOS 14.0, *) {
            viewController = GKGameCenterViewController(state: .achievements)
        } else {
            viewController = GKGameCenterViewController.init()
            viewController.viewState = .achievements
        }
        
        viewController.gameCenterDelegate = self
        UnityGetGLViewController().present(viewController, animated: true)
    }
    
    @objc public func CORShowLeaderboards()
    {
        let viewController : GKGameCenterViewController
        if #available(iOS 14.0, *) {
            viewController = GKGameCenterViewController(state: .leaderboards)
        } else {
            viewController = GKGameCenterViewController.init()
            viewController.viewState = .leaderboards
        }
        
        viewController.gameCenterDelegate = self
        UnityGetGLViewController().present(viewController, animated: true)
    }
    
    @objc public func CORShowLeaderboard(leaderboardId:String)
    {
        print("==== CORShowLeaderboard")
        let viewController : GKGameCenterViewController
        if #available(iOS 14.0, *) {
            viewController = GKGameCenterViewController(
                leaderboardID: leaderboardId,
                playerScope: .global,
                timeScope: .allTime)
        } else {
            viewController = GKGameCenterViewController.init()
            viewController.leaderboardIdentifier = leaderboardId
            viewController.leaderboardTimeScope = GKLeaderboard.TimeScope.allTime
        }
        viewController.gameCenterDelegate = self
        UnityGetGLViewController().present(viewController, animated: true)
    }
    
    @objc public func CORReportLeaderboardScore (leaderboardId:String, score:Int)
    {
        if #available(iOS 14.0, *) {
            GKLeaderboard.submitScore(
                score,
                context: 0,
                player: GKLocalPlayer.local,
                leaderboardIDs: [leaderboardId]
            ) { error in
                print("Error while reporting leaderboard: \(String(describing: error))")
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc public func CORGetWorldRank (leaderboardId:String)
    {
        // Display scores for a specific leaderboard.
        let viewController : GKGameCenterViewController
        
        if #available(iOS 14.0, *) {
            viewController = GKGameCenterViewController(leaderboardID: leaderboardId,
                                                        playerScope: .global,
                                                        timeScope: .allTime)
        } else {
            viewController = GKGameCenterViewController.init()
            viewController.viewState = .leaderboards
            viewController.leaderboardIdentifier = leaderboardId
            viewController.leaderboardTimeScope = .allTime
        }
        
        viewController.gameCenterDelegate = self
        UnityGetGLViewController().present(viewController, animated: true, completion: nil)
    }
    
    @objc public func CORGetAchievementStatus (achievementId:String) -> Bool
    {
        var achievement: GKAchievement? = nil

        let achievementID = achievementId

        // Find an existing achievement.
        achievement = Achievements?.first(where: { $0.identifier == achievementID})
        if(achievement==nil)
        {
            //print("DEGIE NO achievementID:" , achievementID as Any)
            return false;
        }
        
        //print("DEGIE YES achievementID:" , achievementID as Any)
        //print("DEGIE YES percentComplete:" , achievement?.percentComplete as Any)

        return achievement?.percentComplete == 100
    }
    
    
    @objc public func CORGetAchievements (callback: @escaping (Int32) -> Void)
    {
        // Load the player's active achievements.
        GKAchievement.loadAchievements(completionHandler: { (achievements: [GKAchievement]?, error: Error?) in
            
            print("DEGIE count.", achievements?.count as Any)
            
            self.Achievements = achievements.map{ $0 }
            
            print("DEGIE Achievements count.", self.Achievements?.count as Any)

            if error != nil 
            {
                // Handle the error that occurs.
                print("DEGIE Error: \(String(describing: error))")
                callback(0)
            } else {
                callback(1)
            }
        })
    }
}
