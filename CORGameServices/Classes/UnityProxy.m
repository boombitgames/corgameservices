//
//  UnityProxy.m
//  CORGameServices
//
//  Created by Tomasz Wasilewski on 19/03/2024.
//

#include "UnityProxy.h"
#import <Foundation/Foundation.h>
#import <CORGameServices/CORGameServices-Swift.h>

//typedef void (*GameServicesCallback)(int);
FOUNDATION_EXPORT void _CORLogIn(GameServicesCallback callback)
{
    [[CORGameServices shared] loginWithCallback:^(int32_t loginStatus){ callback(loginStatus);}];
}
FOUNDATION_EXPORT void _CORGetAchievements(GameServicesCallback callback)
{
    [[CORGameServices shared] CORGetAchievementsWithCallback:^(int32_t achievementsStatus){ callback(achievementsStatus);}];
}
char* _CORDisplayName(void)
{
    return MakeStringCopy([[CORGameServices shared]getDisplayName]);
}
bool _CORIsLoggedIn(void)
{
    return [[CORGameServices shared]isLoggedIn];
}
char* _CORPlayerID(void)
{
    return MakeStringCopy([[CORGameServices shared]getPlayerID]);
}
char* _CORGamePlayerID(void)
{
    return MakeStringCopy([[CORGameServices shared]getGamePlayerID]);
}
char* _CORTeamPlayerID(void)
{
    return MakeStringCopy([[CORGameServices shared]getTeamPlayerID]);
}
void _CORReportAchievementProgress(const char* achievement, double progress)
{
    [[CORGameServices shared]reportAchievementProgressWithAchievementId:@(achievement) progress:progress];
}
void _CORResetAllAchievements(void)
{
    [[CORGameServices shared]CORResetAllAchievements];
}
void _CORShowAchievements(void)
{
    [[CORGameServices shared]CORShowAchievements];
}
void _CORShowLeaderboards(void)
{
    [[CORGameServices shared]CORShowLeaderboards];
}
void _CORShowLeaderboard(const char* leaderboardId)
{
    [[CORGameServices shared]CORShowLeaderboardWithLeaderboardId:@(leaderboardId)];
}
void _CORReportLeaderboardScore(const char* leaderboard, long score)
{
    [[CORGameServices shared]CORReportLeaderboardScoreWithLeaderboardId:@(leaderboard) score:score];
}
void _CORGetWorldRank(const char* leaderboard)
{
    [[CORGameServices shared]CORGetWorldRankWithLeaderboardId:@(leaderboard)];
}
bool _CORGetAchievementStatus(const char* achievementId)
{
    return [[CORGameServices shared] CORGetAchievementStatusWithAchievementId:@(achievementId)];
}
