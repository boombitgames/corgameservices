//
//  UnityProxy.h
//  Pods
//
//  Created by Tomasz Wasilewski on 19/03/2024.
//

#ifndef UnityProxy_h
#define UnityProxy_h

#include <CORCommon/CORCommon.h>

typedef void (*GameServicesCallback)(int);
void CORLogIn(GameServicesCallback callback);
void CORGetAchievements(GameServicesCallback callback);
bool CORIsLoggedIn(void);
char* CORPlayerID(void);
char* CORGamePlayerID(void);
void CORReportAchievementProgress(const char* achievement, double progress);
void CORResetAllAchievements(void);
void CORShowAchievements(void);
void CORReportLeaderboardScore(const char* leaderboard, long score);
void CORGetWorldRank(const char* leaderboard);
bool CORGetAchievementStatus(const char* achievementId);
#endif /* UnityProxy_h */
