# CORGameServices

[![Version](https://img.shields.io/cocoapods/v/CORGameServices.svg?style=flat)](https://cocoapods.org/pods/CORGameServices)
[![License](https://img.shields.io/cocoapods/l/CORGameServices.svg?style=flat)](https://cocoapods.org/pods/CORGameServices)
[![Platform](https://img.shields.io/cocoapods/p/CORGameServices.svg?style=flat)](https://cocoapods.org/pods/CORGameServices)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CORGameServices is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CORGameServices'
```

## Author

core@boombit.com

## License

CORGameServices is available under the MIT license. See the LICENSE file for more info.
